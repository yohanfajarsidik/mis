---
title: Hello World
subtitle: 
date: 2018-01-06
tags: ["news"]
---

Website ini berasal dari [template] yang
kemudian dikembangkan dan dikelola sendiri oleh Yohan Sidik. 

*Content* blog tidak secara *real time* ditulis, melainkan
ditulis dan dipost secara offline terlebih dahulu menggunakan
*localhost*. 

Pada halaman ini mungkin saja akan tertampil hal-hal dan gambar-gambar 
yang tidak sesuai dikarenakan website ini masih dalam proses pengembangan.

Seperti gambar berikut ini. Gambar tersebut hanya berupa *test*. 

![test](https://gitlab.com/yohanfajarsidik/mis/raw/master/images/circuit.png)

Contoh math di web ini

 
<img src="https://tex.s2cms.ru/svg/%5Cfrac%7B1%7D%7B2%7D" alt="\frac{1}{2}" />




[template]: https://gitlab.com/pages