---
title: English Pronunciation
subtitle: 
date: 2018-01-06
tags: ["English"]
---

The spoken English and the written English might be 
different. The needed tool to study how to correctly pronounce the English
words is called International Phonetic Alphabet [IPA]. This tool is explained by [mmmEnglish] and [Fluent Forever] in Youtube. 


[IPA]: https://en.wikipedia.org/wiki/International_Phonetic_Alphabet
[mmmEnglish]: https://www.youtube.com/watch?v=n4NVPg2kHv4
[Fluent Forever]: https://www.youtube.com/watch?v=-e66ByetpDY